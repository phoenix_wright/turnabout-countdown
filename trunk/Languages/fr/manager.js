{
"trial_manager" : "Gestionnaire de procès",

"own_trials" : "Vos procès",
"collaborating_trials" : "Procès auxquels vous collaborez",

"independant_trials" : "Procès indépendants",
"trial_sequences" : "Séries",

"open_editor" : "Ouvrir dans l'éditeur",
"save" : "Sauvegarder"
}
