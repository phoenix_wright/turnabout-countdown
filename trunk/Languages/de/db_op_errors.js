{
"error_perms_file_edit" : "Kann Falldatei nicht bearbeiten",
"error_perms_file_delete" : "Kann Falldatei nicht löschen",

"error_perms_trial_read" : "Unzureichende Benutzerrechte, um den Fall abzuspielen",
"error_perms_trial_manage_backups" : "Unzureichende Benutzerrechte, um Backups zu verwalten",
"error_perms_trial_edit_secured_metadata" : "Unzureichende Benutzerrechte, um auf gesicherte Fall-Metadaten zuzugreifen",
"error_perms_trial_edit_metadata" : "Unzureichende Benutzerrechte, um Fall-Metadaten zu bearbeiten",
"error_perms_trial_create_someone_else" : "Unzureichende Benutzerrechte, um Fall für einen anderen User anzulegen",

"error_perms_sequence_add_trial" : "Unzureichende Benutzerrechte, um diesen Fall zu einer Sequenz hinzuzufügen",
"error_perms_sequence_add_sequence" : "Unzureichende Benutzerrechte, um diese Sequenz zu einer Sequenz hinzuzufügen",
"error_perms_sequence_edit" : "Unzureichende Benutzerrechte, um diese Sequenz zu bearbeiten",
"error_perms_sequence_delete" : "Unzureichende Benutzerrechte, um diese Sequenz zu löschen",


"error_user_inactive": "Nur ein angemeldeter Benutzer kann diese Aktion ausführen.",

"error_user_trial_language_unavailable" : "Die gewählte Sprache ist nicht verfügbar.",
"error_user_trial_backup_no_slot" : "Du hast keine Backup-Slots mehr",

"error_user_sequence_trial_already_taken" : "Dieser Fall gehört bereits zu einer anderen Sequenz",
"error_user_sequence_sequence_already_taken" : "Diese Sequenz ist bereits einer anderen Sequenz untergeordnet",


"error_tech_v5_trial_language_edit_in_sequence" : "Die Sprache eines Falles in einer Sequenz kann nicht verändert werden",
"error_tech_v5_trial_author_edit_in_sequence" : "Der Autor eines Falles in einer Sequenz kann nicht verändert werden",

"error_tech_v5_sequence_id_already_used" : "Eine Sequenz mit diesem Namen existiert bereits",
"error_tech_v5_sequence_empty" : "Eine Sequenz muss mindestens einen Fall enthalten",
"error_tech_v5_sequence_incorrect_trial_author" : "Eine Sequenz kann nur Fälle mit dem gleichen Autor enthalten",
"error_tech_v5_sequence_incorrect_trial_language" : "Eine Sequenz kann nur Fälle in der gleichen Sprache enthalten",
"error_tech_v5_sequence_nested" : "Geschachtelte Sequenzen werden nicht unterstützt"
}
